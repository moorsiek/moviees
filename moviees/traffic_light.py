from random import choice
from pyknow import *


def ask(question):
    answer = input(question)
    return answer.strip("\n")


class Light(Fact):
    """Info about the traffic light."""
    pass


class RobotCrossStreet(KnowledgeEngine):
    @Rule(Light(color='green'))
    def green_light(self):
        print("Walk")

    @Rule(Light(color='red'))
    def red_light(self):
        print("Don't walk")

    @Rule(AS.light << Light(color=L('yellow') | L('blinking-yellow')))
    def cautious(self, light):
        print("Be cautious because light is", light["color"])


def run():
    engine = RobotCrossStreet()
    engine.reset()

    colors = {'green', 'yellow', 'blinking-yellow', 'red'}

    while True:
        color = ask('Which color is it ({})?\n'.format(', '.join(colors)))
        if color not in colors:
            print('There\'s no such color!\n')
            continue
        break

    engine.declare(Light(color=color))
    engine.run()


run()
