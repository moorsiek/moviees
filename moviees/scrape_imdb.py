import requests
from bs4 import BeautifulSoup
import os.path


def querySelector(el, selector):
    parts = selector.split(' ')
    results = [el]
    for part in parts:
        args = {}
        if part[0] == '#':
            args['id'] = part[1:]
        elif part[0] == '.':
            cls = parts[1:]
            args['class_'] = cls
        else:
            name = part
            args['name'] = name

        newResults = []
        for result in results:
            res = result.find_all(**args)
            for r in res:
                newResults.append(r)
        results = newResults
    return result


url = 'https://www.imdb.com/title/tt4154756/'
headers = {'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/62.0.3202.75 Chrome/62.0.3202.75 Safari/537.36'}

if os.path.isfile('./page.html'):
    f = open('./page.html', 'r')
    content = f.read()
    f.close()
else:
    r = requests.get(url, headers=headers)
    content = r.text
    f = open('./page.html', 'w')
    f.write(str(content))
    f.close()

#print(content)
soup = BeautifulSoup(content, 'html.parser')

#res = querySelector(soup, '#title-overview-widget > .vital > .title_block > div > .titleBar > .title_wrapper > h1')
res = soup.select('#title-overview-widget > .vital > .title_block > div > .titleBar > .title_wrapper > h1')

#for k in res:
#    print(k)
for r in res:
    for c in r.children:
        if c.name is not None:
            c.extract()
        # print('name = ', c.name)
    # r.clear()
    print(r.text)


